import React, { useState } from "react";
import NoteForm from "./components/NoteForm";
// import NoteList from "./components/NoteList";
import Noted from "./components/Note";
import "./App.css";

interface Note {
  title: string;
  content: string;
  number: number;
}

const App: React.FC = () => {
  const [notes, setNotes] = useState<Note[]>([]);

  const addNote = (note: Note) => {
    setNotes([...notes, note]);
  };

  const updateNote = (
    index: number,
    newTitle: string,
    newContent: string,
    newNumber: number

  ) => {
    const newNotes = [...notes];
    newNotes[index] = { title: newTitle, content: newContent, number: newNumber};
    setNotes(newNotes);
  };

  const removeNote = (index: number) => {
    const newNotes = [...notes];
    newNotes.splice(index, 1);
    setNotes(newNotes);
  };

  return (
    <div className="App">
      <h1>Note-Taking App</h1>
      <NoteForm addNote={addNote} />
      {/* <NoteList notes={notes} updateNote={updateNote} removeNote={removeNote} /> */}
      {notes.map((note, index) => (
        <Noted
          key={index}
          index={index}
          note={note}
          updateNote={updateNote}
          removeNote={removeNote}
        />
      ))}
    </div>
  );
};

export default App;
