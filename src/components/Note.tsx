import React, { useState } from "react";

interface NoteProps {
  note: Note;
  index: number;
  updateNote: (index: number, newTitle: string, newContent: string, newNumber: number) => void;
  removeNote: (index: number) => void;
}

interface Note {
  title: string;
  content: string;
  number: number;
}

const Note: React.FC<NoteProps> = ({ note, index, updateNote, removeNote }) => {
  const [isEditing, setIsEditing] = useState(false);
  const [newTitle, setNewTitle] = useState(note.title);
  const [newContent, setNewContent] = useState(note.content);
  const [newNumber, setNewNumber] = useState(note.number);

  const handleUpdate = () => {
    updateNote(index, newTitle, newContent, newNumber);
    setIsEditing(false);
  };

  return (
    <div className="note">
      {isEditing ? (
        <div>
          <input
            type="text"
            value={newTitle}
            onChange={(e) => setNewTitle(e.target.value)}
            placeholder="Note title"
          />
          <textarea
            value={newContent}
            onChange={(e) => setNewContent(e.target.value)}
            placeholder="Note content"
          ></textarea>
          <input
            type="number"
            value={newNumber}
            onChange={(e) => setNewNumber(Number(e.target.value))}
            placeholder="Note title"
          />
          <button onClick={handleUpdate}>Update</button>
        </div>
      ) : (
        <div>
          <h2>{note.title}</h2>
          <p>{note.content}</p>
          <p>{note.number}</p>
        </div>
      )}
      <button onClick={() => setIsEditing(true)}>Edit</button>
      <button onClick={() => removeNote(index)}>Remove</button>
    </div>
  );
};

export default Note;
