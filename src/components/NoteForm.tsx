import React, { useState } from "react";

interface NoteFormProps {
  addNote: (note: Note) => void;
}

interface Note {
  title: string;
  content: string;
  number: number;
}

const NoteForm: React.FC<NoteFormProps> = ({ addNote }) => {
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");
  const [numbers, setNumbers] = useState(Number("-"));

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    if (!title || !content || !numbers) return;
    addNote({
      title: title,
      content: content,
      number: numbers,
    });
    setTitle("");
    setContent("");
    setNumbers(Number("-"))
  };

  return (
    <form onSubmit={handleSubmit}>
      <input
        type="text"
        value={title}
        onChange={(e) => setTitle(e.target.value)}
        placeholder="Note title"
      />
      <textarea
        value={content}
        onChange={(e) => setContent(e.target.value)}
        placeholder="Note content"
      ></textarea>
      <input
        type="number"
        value={numbers}
        onChange={(e) => setNumbers(Number(e.target.value))}
        placeholder="Numbers only !"
      />
      <button type="submit">Add Note</button>
    </form>
  );
};

export default NoteForm;
