import React from 'react';
import Noted from './Note';

interface Note {
  title: string;
  content: string;
  number: number
}

interface NoteListProps {
  notes: Note[];
  updateNote: (index: number, newTitle: string, newContent: string) => void;
  removeNote: (index: number) => void;
}

const NoteList: React.FC<NoteListProps> = ({ notes, updateNote, removeNote }) => {
  return (
    <div>
      {notes.map((note, index) => (
        <Noted
          key={index}
          index={index}
          note={note}
          updateNote={updateNote}
          removeNote={removeNote}
        />
      ))}
    </div>
  );
};

export default NoteList;
